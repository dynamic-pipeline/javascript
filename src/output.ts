import { writeFileSync } from 'fs';
import { GitlabRef } from './references/GitlabRef';

export function syncToConsole() {
  console.log(GitlabRef.output());
}

export function sync(fileName = 'pipeline.yml') {
  writeFileSync(fileName, GitlabRef.output(), 'utf8');
}
