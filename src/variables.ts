export const {
  /**
   * Number of attempts to download artifacts running a job
   * @gitlabVersion 8.15
   * @runnerVersion 1.9
   */
  ARTIFACT_DOWNLOAD_ATTEMPTS,
  /**
   * Source chat channel which triggered the ChatOps command
   * @gitlabVersion 10.6
   * @runnerVersion all
   */
  CHAT_CHANNEL,
  /**
   * Additional arguments passed in the ChatOps command
   * @gitlabVersion 10.6
   * @runnerVersion all
   */
  CHAT_INPUT,
  /**
   * Mark that job is executed in CI environment
   * @gitlabVersion all
   * @runnerVersion 0.4
   */
  CI,
  /**
   * The GitLab API v4 root URL
   * @gitlabVersion 11.7
   * @runnerVersion all
   */
  CI_API_V4_URL,
  /**
   * Top-level directory where builds are executed.
   * @gitlabVersion all
   * @runnerVersion 11.10
   */
  CI_BUILDS_DIR,
  /**
   * The previous latest commit present on a branch before a merge request. Only populated when there is a merge request associated with the pipeline.
   * @gitlabVersion 11.2
   * @runnerVersion all
   */
  CI_COMMIT_BEFORE_SHA,
  /**
   * The description of the commit: the message without first line, if the title is shorter than 100 characters; full message in other case.
   * @gitlabVersion 10.8
   * @runnerVersion all
   */
  CI_COMMIT_DESCRIPTION,
  /**
   * The full commit message.
   * @gitlabVersion 10.8
   * @runnerVersion all
   */
  CI_COMMIT_MESSAGE,
  /**
   * The branch or tag name for which project is built
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_COMMIT_REF_NAME,
  /**
   * true if the job is running on a protected reference, false if not
   * @gitlabVersion 11.11
   * @runnerVersion all
   */
  CI_COMMIT_REF_PROTECTED,
  /**
   * $CI_COMMIT_REF_NAME lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -. Use in URLs, host names and domain names.
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_COMMIT_REF_SLUG,
  /**
   * The commit revision for which project is built
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_COMMIT_SHA,
  /**
   * The first eight characters of CI_COMMIT_SHA
   * @gitlabVersion 11.7
   * @runnerVersion all
   */
  CI_COMMIT_SHORT_SHA,
  /**
   * The commit branch name. Present only when building branches.
   * @gitlabVersion 12.6
   * @runnerVersion 0.5
   */
  CI_COMMIT_BRANCH,
  /**
   * The commit tag name. Present only when building tags.
   * @gitlabVersion 9.0
   * @runnerVersion 0.5
   */
  CI_COMMIT_TAG,
  /**
   * The title of the commit - the full first line of the message
   * @gitlabVersion 10.8
   * @runnerVersion all
   */
  CI_COMMIT_TITLE,
  /**
   * Unique ID of build execution within a single executor.
   * @gitlabVersion all
   * @runnerVersion 11.10
   */
  CI_CONCURRENT_ID,
  /**
   * Unique ID of build execution within a single executor and project.
   * @gitlabVersion all
   * @runnerVersion 11.10
   */
  CI_CONCURRENT_PROJECT_ID,
  /**
   * The path to CI configuration file. Defaults to .gitlab-ci.yml
   * @gitlabVersion 9.4
   * @runnerVersion 0.5
   */
  CI_CONFIG_PATH,
  /**
   * Whether debug logging (tracing) is enabled
   * @gitlabVersion all
   * @runnerVersion 1.7
   */
  CI_DEBUG_TRACE,
  /**
   * The name of the default branch for the project.
   * @gitlabVersion 12.4
   * @runnerVersion all
   */
  CI_DEFAULT_BRANCH,
  /**
   * Authentication password of the GitLab Deploy Token, only present if the Project has one related.
   * @gitlabVersion 10.8
   * @runnerVersion all
   */
  CI_DEPLOY_PASSWORD,
  /**
   * Authentication username of the GitLab Deploy Token, only present if the Project has one related.
   * @gitlabVersion 10.8
   * @runnerVersion all
   */
  CI_DEPLOY_USER,
  /**
   * Marks that the job is executed in a disposable environment (something that is created only for this job and disposed of/destroyed after the execution - all executors except shell and ssh). If the environment is disposable, it is set to true, otherwise it is not defined at all.
   * @gitlabVersion all
   * @runnerVersion 10.1
   */
  CI_DISPOSABLE_ENVIRONMENT,
  /**
   * The name of the environment for this job. Only present if environment:name is set.
   * @gitlabVersion 8.15
   * @runnerVersion all
   */
  CI_ENVIRONMENT_NAME,
  /**
   * A simplified version of the environment name, suitable for inclusion in DNS, URLs, Kubernetes labels, etc. Only present if environment:name is set.
   * @gitlabVersion 8.15
   * @runnerVersion all
   */
  CI_ENVIRONMENT_SLUG,
  /**
   * The URL of the environment for this job. Only present if environment:url is set.
   * @gitlabVersion 9.3
   * @runnerVersion all
   */
  CI_ENVIRONMENT_URL,
  /**
   * Pull Request ID from GitHub if the pipelines are for external pull requests. Available only if only: [external_pull_requests] or rules syntax is used and the pull request is open.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_EXTERNAL_PULL_REQUEST_IID,
  /**
   * The source branch name of the pull request if the pipelines are for external pull requests. Available only if only: [external_pull_requests] or rules syntax is used and the pull request is open.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME,
  /**
   * The HEAD SHA of the source branch of the pull request if the pipelines are for external pull requests. Available only if only: [external_pull_requests] or rules syntax is used and the pull request is open.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA,
  /**
   * The target branch name of the pull request if the pipelines are for external pull requests. Available only if only: [external_pull_requests] or rules syntax is used and the pull request is open.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME,
  /**
   * The HEAD SHA of the target branch of the pull request if the pipelines are for external pull requests. Available only if only: [external_pull_requests] or rules syntax is used and the pull request is open.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA,
  /**
   * The unique ID of the current job that GitLab CI/CD uses internally
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_JOB_ID,
  /**
   * The name of the image running the CI job
   * @gitlabVersion 12.9
   * @runnerVersion 12.9
   */
  CI_JOB_IMAGE,
  /**
   * The flag to indicate that job was manually started
   * @gitlabVersion 8.12
   * @runnerVersion all
   */
  CI_JOB_MANUAL,
  /**
   * The name of the job as defined in .gitlab-ci.yml
   * @gitlabVersion 9.0
   * @runnerVersion 0.5
   */
  CI_JOB_NAME,
  /**
   * The name of the stage as defined in .gitlab-ci.yml
   * @gitlabVersion 9.0
   * @runnerVersion 0.5
   */
  CI_JOB_STAGE,
  /**
   * Token used for authenticating with the GitLab Container Registry and downloading dependent repositories
   * @gitlabVersion 9.0
   * @runnerVersion 1.2
   */
  CI_JOB_TOKEN,
  /**
   * RS256 JSON web token that can be used for authenticating with third party systems that support JWT authentication, for example HashiCorp’s Vault.
   * @gitlabVersion 12.10
   * @runnerVersion all
   */
  CI_JOB_JWT,
  /**
   * Job details URL
   * @gitlabVersion 11.1
   * @runnerVersion 0.5
   */
  CI_JOB_URL,
  /**
   * Included with the value true only if the pipeline has a Kubernetes cluster available for deployments. Not included if no cluster is available. Can be used as an alternative to only:kubernetes/except:kubernetes with rules:if
   * @gitlabVersion 13.0
   * @runnerVersion all
   */
  CI_KUBERNETES_ACTIVE,
  /**
   * Comma-separated list of username(s) of assignee(s) for the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_ASSIGNEES,
  /**
   * The ID of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_ID,
  /**
   * The IID of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_IID,
  /**
   * Comma-separated label names of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_LABELS,
  /**
   * The milestone title of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_MILESTONE,
  /**
   * The ID of the project of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_PROJECT_ID,
  /**
   * The path of the project of the merge request if the pipelines are for merge requests (e.g. namespace/awesome-project). Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_PROJECT_PATH,
  /**
   * The URL of the project of the merge request if the pipelines are for merge requests (e.g. http://192.168.10.15:3000/namespace/awesome-project). Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_PROJECT_URL,
  /**
   * The ref path of the merge request if the pipelines are for merge requests. (e.g. refs/merge-requests/1/head). Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_REF_PATH,
  /**
   * The source branch name of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,
  /**
   * The HEAD SHA of the source branch of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used, the merge request is created, and the pipeline is a merged result pipeline.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_SOURCE_BRANCH_SHA,
  /**
   * The ID of the source project of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_SOURCE_PROJECT_ID,
  /**
   * The path of the source project of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_SOURCE_PROJECT_PATH,
  /**
   * The URL of the source project of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_SOURCE_PROJECT_URL,
  /**
   * The target branch name of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.6
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_TARGET_BRANCH_NAME,
  /**
   * The HEAD SHA of the target branch of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used, the merge request is created, and the pipeline is a merged result pipeline.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_TARGET_BRANCH_SHA,
  /**
   * The title of the merge request if the pipelines are for merge requests. Available only if only: [merge_requests] or rules syntax is used and the merge request is created.
   * @gitlabVersion 11.9
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_TITLE,
  /**
   * The event type of the merge request, if the pipelines are for merge requests. Can be detached, merged_result or merge_train.
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_MERGE_REQUEST_EVENT_TYPE,
  /**
   * Index of the job in the job set. If the job is not parallelized, this variable is not set.
   * @gitlabVersion 11.5
   * @runnerVersion all
   */
  CI_NODE_INDEX,
  /**
   * Total number of instances of this job running in parallel. If the job is not parallelized, this variable is set to 1.
   * @gitlabVersion 11.5
   * @runnerVersion all
   */
  CI_NODE_TOTAL,
  /**
   * The configured domain that hosts GitLab Pages.
   * @gitlabVersion 11.8
   * @runnerVersion all
   */
  CI_PAGES_DOMAIN,
  /**
   * URL to GitLab Pages-built pages. Always belongs to a subdomain of CI_PAGES_DOMAIN.
   * @gitlabVersion 11.8
   * @runnerVersion all
   */
  CI_PAGES_URL,
  /**
   * The unique ID of the current pipeline that GitLab CI/CD uses internally
   * @gitlabVersion 8.10
   * @runnerVersion all
   */
  CI_PIPELINE_ID,
  /**
   * The unique ID of the current pipeline scoped to project
   * @gitlabVersion 11.0
   * @runnerVersion all
   */
  CI_PIPELINE_IID,
  /**
   * Indicates how the pipeline was triggered. Possible options are: push, web, trigger, schedule, api, pipeline, parent_pipeline, external, chat, merge_request_event, and external_pull_request_event. For pipelines created before GitLab 9.5, this will show as unknown
   * @gitlabVersion 10.0
   * @runnerVersion all
   */
  CI_PIPELINE_SOURCE,
  /**
   * The flag to indicate that job was triggered
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_PIPELINE_TRIGGERED,
  /**
   * Pipeline details URL
   * @gitlabVersion 11.1
   * @runnerVersion 0.5
   */
  CI_PIPELINE_URL,
  /**
   * The full path where the repository is cloned and where the job is run. If the GitLab Runner builds_dir parameter is set, this variable is set relative to the value of builds_dir. For more information, see Advanced configuration for GitLab Runner.
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_PROJECT_DIR,
  /**
   * The unique ID of the current project that GitLab CI/CD uses internally
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_PROJECT_ID,
  /**
   * The name of the directory for the project that is currently being built. For example, if the project URL is gitlab.example.com/group-name/project-1, the CI_PROJECT_NAME would be project-1.
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_PROJECT_NAME,
  /**
   * The project namespace (username or group name) that is currently being built
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_PROJECT_NAMESPACE,
  /**
   * The namespace with project name
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_PROJECT_PATH,
  /**
   * $CI_PROJECT_PATH lowercased and with everything except 0-9 and a-z replaced with -. Use in URLs and domain names.
   * @gitlabVersion 9.3
   * @runnerVersion all
   */
  CI_PROJECT_PATH_SLUG,
  /**
   * Comma-separated, lowercased list of the languages used in the repository (e.g. ruby,javascript,html,css)
   * @gitlabVersion 12.3
   * @runnerVersion all
   */
  CI_PROJECT_REPOSITORY_LANGUAGES,
  /**
   * The human-readable project name as displayed in the GitLab web interface.
   * @gitlabVersion 12.4
   * @runnerVersion all
   */
  CI_PROJECT_TITLE,
  /**
   * The HTTP(S) address to access project
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_PROJECT_URL,
  /**
   * The project visibility (internal, private, public)
   * @gitlabVersion 10.3
   * @runnerVersion all
   */
  CI_PROJECT_VISIBILITY,
  /**
   * If the Container Registry is enabled it returns the address of GitLab’s Container Registry. This variable will include a :port value if one has been specified in the registry configuration.
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_REGISTRY,
  /**
   * If the Container Registry is enabled for the project it returns the address of the registry tied to the specific project
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_REGISTRY_IMAGE,
  /**
   * The password to use to push containers to the GitLab Container Registry
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_REGISTRY_PASSWORD,
  /**
   * The username to use to push containers to the GitLab Container Registry
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_REGISTRY_USER,
  /**
   * The URL to clone the Git repository
   * @gitlabVersion 9.0
   * @runnerVersion all
   */
  CI_REPOSITORY_URL,
  /**
   * The description of the runner as saved in GitLab
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_RUNNER_DESCRIPTION,
  /**
   * The OS/architecture of the GitLab Runner executable (note that this is not necessarily the same as the environment of the executor)
   * @gitlabVersion all
   * @runnerVersion 10.6
   */
  CI_RUNNER_EXECUTABLE_ARCH,
  /**
   * The unique ID of runner being used
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_RUNNER_ID,
  /**
   * GitLab Runner revision that is executing the current job
   * @gitlabVersion all
   * @runnerVersion 10.6
   */
  CI_RUNNER_REVISION,
  /**
   * First eight characters of GitLab Runner’s token used to authenticate new job requests. Used as Runner’s unique ID
   * @gitlabVersion all
   * @runnerVersion 12.3
   */
  CI_RUNNER_SHORT_TOKEN,
  /**
   * The defined runner tags
   * @gitlabVersion 8.10
   * @runnerVersion 0.5
   */
  CI_RUNNER_TAGS,
  /**
   * GitLab Runner version that is executing the current job
   * @gitlabVersion all
   * @runnerVersion 10.6
   */
  CI_RUNNER_VERSION,
  /**
   * Mark that job is executed in CI environment
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_SERVER,
  /**
   * The base URL of the GitLab instance, including protocol and port (like https://gitlab.example.com:8080)
   * @gitlabVersion 12.7
   * @runnerVersion all
   */
  CI_SERVER_URL,
  /**
   * Host component of the GitLab instance URL, without protocol and port (like gitlab.example.com)
   * @gitlabVersion 12.1
   * @runnerVersion all
   */
  CI_SERVER_HOST,
  /**
   * Port component of the GitLab instance URL, without host and protocol (like 3000)
   * @gitlabVersion 12.8
   * @runnerVersion all
   */
  CI_SERVER_PORT,
  /**
   * Protocol component of the GitLab instance URL, without host and port (like https)
   * @gitlabVersion 12.8
   * @runnerVersion all
   */
  CI_SERVER_PROTOCOL,
  /**
   * The name of CI server that is used to coordinate jobs
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_SERVER_NAME,
  /**
   * GitLab revision that is used to schedule jobs
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_SERVER_REVISION,
  /**
   * GitLab version that is used to schedule jobs
   * @gitlabVersion all
   * @runnerVersion all
   */
  CI_SERVER_VERSION,
  /**
   * GitLab version major component
   * @gitlabVersion 11.4
   * @runnerVersion all
   */
  CI_SERVER_VERSION_MAJOR,
  /**
   * GitLab version minor component
   * @gitlabVersion 11.4
   * @runnerVersion all
   */
  CI_SERVER_VERSION_MINOR,
  /**
   * GitLab version patch component
   * @gitlabVersion 11.4
   * @runnerVersion all
   */
  CI_SERVER_VERSION_PATCH,
  /**
   * Marks that the job is executed in a shared environment (something that is persisted across CI invocations like shell or ssh executor). If the environment is shared, it is set to true, otherwise it is not defined at all.
   * @gitlabVersion all
   * @runnerVersion 10.1
   */
  CI_SHARED_ENVIRONMENT,
  /**
   * Number of attempts to fetch sources running a job
   * @gitlabVersion 8.15
   * @runnerVersion 1.9
   */
  GET_SOURCES_ATTEMPTS,
  /**
   * Mark that job is executed in GitLab CI/CD environment
   * @gitlabVersion all
   * @runnerVersion all
   */
  GITLAB_CI,
  /**
   * The comma separated list of licensed features available for your instance and plan
   * @gitlabVersion 10.6
   * @runnerVersion all
   */
  GITLAB_FEATURES,
  /**
   * The email of the user who started the job
   * @gitlabVersion 8.12
   * @runnerVersion all
   */
  GITLAB_USER_EMAIL,
  /**
   * The ID of the user who started the job
   * @gitlabVersion 8.12
   * @runnerVersion all
   */
  GITLAB_USER_ID,
  /**
   * The login username of the user who started the job
   * @gitlabVersion 10.0
   * @runnerVersion all
   */
  GITLAB_USER_LOGIN,
  /**
   * The real name of the user who started the job
   * @gitlabVersion 10.0
   * @runnerVersion all
   */
  GITLAB_USER_NAME,
  /**
   * Number of attempts to restore the cache running a job
   * @gitlabVersion 8.15
   * @runnerVersion 1.9
   */
  RESTORE_CACHE_ATTEMPTS,
} = process.env as Record<string, string>;
