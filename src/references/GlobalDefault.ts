import { Image, Services, Tags, Artifacts, Retry, Cache } from './models';
import { GitlabRef } from './GitlabRef';

export type GlobalDefaultSpec = {
  /** Global docker image */
  image?: Image;
  services?: Services;
  before_script?: Array<string>;
  after_script?: Array<string>;
  tags?: Tags;
  cache?: Cache;
  artifacts?: Artifacts;
  retry?: Retry;
  timeout?: string;
  interruptible?: boolean;
};

export class GlobalDefault extends GitlabRef {
  constructor(public spec: GlobalDefaultSpec) {
    super();
  }
}
