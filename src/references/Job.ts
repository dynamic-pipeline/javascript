import {
  Image,
  Services,
  Rules,
  Needs,
  Tags,
  When,
  Environment,
  Artifacts,
  Retry,
  Trigger,
  Cache,
} from './models';
import { Stage } from './Stage';
import { GitlabRef } from './GitlabRef';

export type JobSpec = {
  /** Used to specify a Docker image to use for the job. */
  image?: Image;
  /** Used to specify a service Docker image, linked to a base image specified in image. */
  services?: Services;
  /** Script is the only required keyword that a job needs. It’s a shell script which is executed by the Runner */
  // TODO: Allow script to be serialized JS
  script: string | Array<string>;
  /** before_script is used to define a command that should be run before each job, including deploy jobs, but after the restoration of any artifacts. This must be an array. */
  before_script?: Array<string>;
  /** after_script is used to define the command that will be run after each job, including failed ones. This must be an array. */
  after_script?: Array<string>;
  /** stage is defined per-job and relies on stages which is defined globally. It allows to group jobs into different stages, and jobs of the same stage are executed in parallel */
  stage?: string | Stage;
  /** extends defines entry names that a job that uses extends is going to inherit from. */
  extends?: string | Job | Array<string | Job>;
  /** rules allows for a list of individual rule objects to be evaluated in order, until one matches and dynamically provides attributes to the job. */
  rules?: Rules;
  /** the needs: keyword enables executing jobs out-of-order, allowing you to implement a directed acyclic graph in your .gitlab-ci.yml. */
  needs?: Needs;
  /** tags is used to select specific Runners from the list of all Runners that are allowed to run this project */
  tags?: Tags;
  /** allow_failure allows a job to fail without impacting the rest of the CI suite. The default value is false, except for manual jobs using the when: manual syntax, unless using rules: syntax, where all jobs default to false, including when: manual jobs. */
  allow_failure?: boolean;
  when?: When;
  environment?: Environment;
  cache?: Cache;
  artifacts?: Artifacts;
  /** coverage allows you to configure how code coverage will be extracted from the job output. */
  coverage?: string;
  /** retry allows you to configure how many times a job is going to be retried in case of a failure. */
  retry?: Retry;
  /** timeout allows you to configure a timeout for a specific job */
  timeout?: string;
  /** parallel allows you to configure how many instances of a job to run in parallel. */
  parallel?: number;
  trigger?: Trigger;
  /** interruptible is used to indicate that a job should be canceled if made redundant by a newer pipeline run. Defaults to false. This value will only be used if the automatic cancellation of redundant pipelines feature is enabled. */
  interruptible?: boolean;
  /** When the resource_group key is defined for a job in .gitlab-ci.yml, job executions are mutually exclusive across different pipelines for the same project. If multiple jobs belonging to the same resource group are enqueued simultaneously, only one of the jobs will be picked by the Runner, and the other jobs will wait until the resource_group is free. */
  resource_group?: string;
  /** GitLab CI/CD allows you to define variables inside .gitlab-ci.yml that are then passed in the job environment. They can be set globally and per-job. When the variables keyword is used on a job level, it will override the global YAML variables and predefined ones of the same name. */
  variables?: Record<string, string> & {
    /** You can set the GIT_STRATEGY used for getting recent application code, either globally or per-job in the variables section. If left unspecified, the default from project settings will be used. */
    GIT_STRATEGY?: 'clone' | 'fetch' | 'none';
    /** You can specify the depth of fetching and cloning using GIT_DEPTH. This allows shallow cloning of the repository which can significantly speed up cloning for repositories with a large number of commits or old, large binaries. The value is passed to git fetch and git clone. */
    GIT_DEPTH?: string;
    /** By default, GitLab Runner clones the repository in a unique subpath of the $CI_BUILDS_DIR directory. However, your project might require the code in a specific directory (Go projects, for example) */
    GIT_CLONE_PATH?: string;
  };
};

export type JobOptions = {
  /** If this job should be hidden (extendable jobs) */
  hidden?: boolean;
};

// TODO: Add a .toString method to make serializing to YAML easier
export class Job extends GitlabRef {
  constructor(public name: string, public spec: JobSpec, public options?: JobOptions) {
    super();
  }
}
