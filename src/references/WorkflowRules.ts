import { Rules } from './models';
import { GitlabRef } from './GitlabRef';

export type WorkflowRulesSpec = Rules;

export class WorkflowRules extends GitlabRef {
  constructor(public spec: WorkflowRulesSpec) {
    super();
  }
}
