import { Job } from './Job';

export type Image =
  | string
  | {
      name: string;
      entrypoint?: string;
    };

export type Service = {
  name?: string;
  alias?: string;
  entrypoint?: string;
  command?: string;
};
export type Services = Array<Service>;

export type Tags = Array<string>;

/** when is used to implement jobs that are run in case of failure or despite the failure. */
export type When = 'on_success' | 'on_failure' | 'delayed' | 'manual' | 'always';

export type Rule = {
  /** rules:if differs slightly from only:variables by accepting only a single expression string, rather than an array of them. Any set of expressions to be evaluated should be conjoined into a single expression using && or ||, and use the variable matching syntax. */
  if?: string;
  /** rules: changes works exactly the same way as only: changes and except: changes, accepting an array of paths. Similarly, it will always return true if there is no Git push event. */
  changes?: Array<string>;
  /** exists accepts an array of paths and will match if any of these paths exist as files in the repository. */
  exists?: Array<string>;
  allow_failure?: boolean;
  start_in?: string;
  when?: When;
};

export type Rules = Array<Rule>;

export type Need =
  | string
  | Job
  | {
      job: string | Job;
      artifacts?: boolean;
      project?: string;
      ref?: string;
    }
  | { pipeline: string };

export type Needs = Array<Need>;

/** environment is used to define that a job deploys to a specific environment. If environment is specified and no environment under that name exists, a new one will be created automatically. */
export type Environment =
  | string
  | {
      name: string;
      /** This is an optional value that when set, it exposes buttons in various places in GitLab which when clicked take you to the defined URL. */
      url?: string;
      /** Closing (stopping) environments can be achieved with the on_stop keyword defined under environment. It declares a different job that runs in order to close the environment. */
      on_stop?: string | Job;
      /** The action keyword is to be used in conjunction with on_stop and is defined in the job that is called to close the environment. */
      action?: 'stop';
      /** The auto_stop_in keyword is for specifying life period of the environment, that when expired, GitLab automatically stops them. */
      auto_stop_in?: string;
      /** The kubernetes block is used to configure deployments to a Kubernetes cluster that is associated with your project. */
      kubernetes?: {
        namespace: string;
      };
    };

/** Use the paths directive to choose which files or directories will be cached. Paths are relative to the project directory ($CI_PROJECT_DIR) and can’t directly link outside it. */
export type Cache = {
  paths: Array<string>;
  untracked?: boolean;
  policy?: 'pull' | 'push' | 'pull-push';
  key?:
    | string
    | {
        /** The cache:key:files keyword extends the cache:key functionality by making it easier to reuse some caches, and rebuild them less often, which will speed up subsequent pipeline runs. */
        files: Array<string>;
        /** The prefix parameter adds extra functionality to key:files by allowing the key to be composed of the given prefix combined with the SHA computed for cache:key:files */
        prefix?: string;
      };
};

/** artifacts is used to specify a list of files and directories which should be attached to the job when it succeeds, fails, or always. */
export type Artifacts = {
  /** Paths are relative to the project directory ($CI_PROJECT_DIR) and can’t directly link outside it. */
  paths: Array<string>;
  /** exclude makes it possible to prevent files from being added to an artifacts archive. */
  exclude?: Array<string>;
  /** The expose_as keyword can be used to expose job artifacts in the merge request UI. */
  expose_as?: string;
  /** The name directive allows you to define the name of the created artifacts archive. That way, you can have a unique name for every archive which could be useful when you’d like to download the archive from GitLab. The artifacts:name variable can make use of any of the predefined variables. The default name is artifacts, which becomes artifacts.zip when downloaded. */
  name?: string;
  /** artifacts:untracked is used to add all Git untracked files as artifacts (along to the paths defined in artifacts:paths). */
  untracked?: boolean;
  /** artifacts:when is used to upload artifacts on job failure or despite the failure. */
  when?: 'on_success' | 'on_failure' | 'always';
  /** expire_in allows you to specify how long artifacts should live before they expire and are therefore deleted, counting from the time they are uploaded and stored on GitLab. */
  expire_in?: string;
  // TODO: Add reports https://docs.gitlab.com/ee/ci/yaml/#artifactsreports
};

export type Retry =
  | number
  | {
      max: number;
      when: string | Array<string>;
    };

/** trigger allows you to define downstream pipeline trigger. When a job created from trigger definition is started by GitLab, a downstream pipeline gets created. */
export type Trigger =
  | string
  | {
      project: string;
      branch?: string;
      strategy?: 'depend';
      include?:
        | string
        | {
            job: string;
            artifact?: string;
          };
    };
