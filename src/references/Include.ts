import { GitlabRef } from './GitlabRef';

export class Include extends GitlabRef {
  constructor(public spec: any) {
    super();
  }
}
