import yaml from 'yaml';
import indent from 'indent-string';
import { Job, GlobalDefault, Stage, WorkflowRules } from './index';

type Refs = {
  jobs: Array<Job>;
  globalDefaults: Array<GlobalDefault>;
  workflowRules: Array<WorkflowRules>;
};

export const refs: Refs = {
  jobs: [],
  globalDefaults: [],
  workflowRules: [],
};

type Json = string | boolean | number | null | undefined | { [key: string]: Json } | Array<Json>;
function specToYaml(spec: Json) {
  return yaml.stringify(spec);
}

function specToJson(spec: Job['spec']): Json {
  const stage = spec.stage instanceof Stage ? spec.stage.name : spec.stage;
  const needs = spec.needs?.map((need) => {
    if (need instanceof Job) {
      return need.name;
    }
    if (typeof need === 'object') {
      // @ts-ignore
      if (need.job instanceof Job) {
        // @ts-ignore
        need.job = need.job.name;
      }
    }
    return need;
  });

  const jobExtends =
    // eslint-disable-next-line no-nested-ternary
    spec.extends instanceof Job
      ? spec.extends.name
      : Array.isArray(spec.extends)
      ? spec.extends.map((jobExtend) => {
          if (jobExtend instanceof Job) {
            return jobExtend.name;
          }
          return jobExtend;
        })
      : spec.extends;

  const environment =
    typeof spec.environment === 'string' || spec.environment == null
      ? spec.environment
      : {
          ...spec.environment,
          on_stop:
            spec.environment?.on_stop instanceof Job
              ? spec.environment.on_stop.name
              : spec.environment?.on_stop,
        };

  const jsonSpec = {
    ...spec,
  };

  if (stage != null) {
    jsonSpec.stage = stage;
  }
  if (needs != null) {
    // @ts-ignore
    jsonSpec.needs = needs;
  }
  if (jobExtends != null) {
    jsonSpec.extends = jobExtends;
  }
  if (environment != null) {
    jsonSpec.environment = environment;
  }

  // @ts-ignore
  return jsonSpec;
}

// TODO: Add a .toString method to make serializing to YAML easier
export class GitlabRef {
  constructor() {
    // Collect Gitlab refs
    if (this instanceof Job) {
      refs.jobs.push(this);
    } else if (this instanceof GlobalDefault) {
      refs.globalDefaults.push(this);
    } else if (this instanceof WorkflowRules) {
      refs.workflowRules.push(this);
    }
  }
  static output() {
    const stages = new Set<string>();

    let stagesLine = '';
    let jobsLine = '';

    refs.jobs.forEach((job) => {
      const { stage } = job.spec;
      if (stage) {
        if (stage instanceof Stage) {
          stages.add(stage.name);
        } else {
          stages.add(stage);
        }
      }

      jobsLine += `${job.name}:
${indent(specToYaml(specToJson(job.spec)), 2)}
`;
    });

    if (stages.size > 0) {
      stagesLine += `stages:\n`;
      stages.forEach((stage) => {
        stagesLine += indent(`- ${stage}\n`, 2);
      });
    }

    return `${stagesLine}
${jobsLine}`;

    // if (this instanceof GlobalDefault) {
    //   refs.globalDefaults.push(this.spec);
    // } else if (this instanceof WorkflowRules) {
    //   refs.workflowRules.push(this.spec);
    // }
  }
}
