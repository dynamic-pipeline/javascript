import { GitlabRef } from './GitlabRef';

// TODO: Add a .toString method to make serializing to YAML easier
export class Stage extends GitlabRef {
  constructor(public name: string) {
    super();
  }
}
