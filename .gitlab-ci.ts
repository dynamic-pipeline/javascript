import { Stage, Job, sync, syncToConsole } from '@dynamic-pipeline/javascript';
import { CI_COMMIT_BRANCH } from '@dynamic-pipeline/javascript/lib/variables';

const installStage = new Stage('install');
const validate = new Stage('validate')
const release = new Stage('release')

const image = 'node:12'
const YARN_CACHE_FOLDER = "$CI_PROJECT_DIR/.cache/yarn"

const install = new Job('install', {
  image,
  stage: installStage,
  script: 'yarn install',
  artifacts: {
    paths: ['node_modules']
  },
  variables: {
    YARN_CACHE_FOLDER,
  },
  cache: {
    key: 'yarn-cache',
    paths: [YARN_CACHE_FOLDER]
  }
})

const build = new Job('build', {
  image,
  stage: validate,
  needs: [install],
  script: 'yarn build',
  artifacts: {
    paths: ['lib']
  }
})

if (CI_COMMIT_BRANCH === 'master') {
  new Job('release', {
    image,
    stage: release,
    needs: [install, build],
    script: [
      'env',
      'git config --global user.name Gabe Meola',
      'git config --global user.email hey@gabe.mx',
      'git config --global push.followTags true',
      'yarn release',
      'git remote set-url origin https://gitlab-ci-token:${GITLAB_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git',
      'git checkout -B ${CI_COMMIT_REF_NAME}',
      'git add .',
      'git commit -m "chore: publish release" || echo "Nothing to commit"',
      'git push -o ci.skip origin ${CI_COMMIT_REF_NAME}'
    ],
  })
}


syncToConsole();
sync();
