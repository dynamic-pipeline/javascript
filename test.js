const { Job, Stage, syncToConsole } = require('./lib');

const initStage = new Stage('init');
const validateStage = new Stage('validate');
const unusedStage = new Stage('unused');

const install = new Job('install', {
  stage: initStage,
  script: 'yarn install',
});

new Job('build', {
  stage: validateStage,
  script: "echo 'hello world'",
  needs: [
    {
      job: install,
      artifacts: true,
    },
  ],
});

syncToConsole();
